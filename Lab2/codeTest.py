import csv
import re

dnslog = open("dns.log")
traff = open("allHost.txt")
dnstraff = csv.reader(dnslog, delimiter="\x09")
count = 0
unwcount = 0
reg = re.compile('[^a-zA-Z.]')
hosts=list()

for line in traff.readlines():
	hosts.append(reg.sub('', line[8:]))

for row in dnstraff:
	try:
		count+=1
		if row[9] in hosts:
			unwcount+=1
			print(row[9])
	except IndexError:
		pass
res = (unwcount/count)*100
print("unwanted traffic = "+str(res) + "%")


dnslog.close()
traff.close()
